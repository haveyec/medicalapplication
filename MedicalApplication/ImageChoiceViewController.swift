//
//  ImageChoiceViewController.swift
//  MedicalApplication
//
//  Created by Havic on 5/19/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//

import UIKit

class ImageChoiceViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    @IBOutlet weak var imageToBeUseInProfile: UIImageView!
    let pickerC = UIImagePickerController()
    let instance = AppConfig.instance
    var data = NSData()

    override func viewDidLoad() {
        super.viewDidLoad()
        pickerC.delegate = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func launchCameraRoll(sender: UIButton) {
     
        
        //Launch UIAlert options for picker
        launchPictureOptions()
        self.presentViewController(pickerC, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        imageToBeUseInProfile.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    @IBAction func saveImage(sender: UIButton) {
        
        guard let image = imageToBeUseInProfile.image else{return}
        
        let imageData = UIImageJPEGRepresentation(image, 1)
        let relativePath = "/image_\(NSDate.timeIntervalSinceReferenceDate()).jpg"
        let path = self.documentsPathForFileName(relativePath)
        let success = imageData!.writeToFile(path, atomically: false)
        
        instance.defaults.setObject(relativePath, forKey: "profileImage")
        goToCreateVC()
        
    }
    
    
    
    //MARK: Helper Functions
    func launchPictureOptions(){
        let alertController = UIAlertController(title: "Question", message: "Where would you like to add a photo from?", preferredStyle: .Alert)
        
        
        let CameraAction = UIAlertAction(title: "Camera", style: .Default) { (action) in
            self.pickerC.sourceType = .Camera
            self.presentViewController(self.pickerC, animated: true, completion: nil)
        }
        alertController.addAction(CameraAction)
        
        let PhotoLibraryAction = UIAlertAction(title: "Photo Library", style: .Default) { (action) in
            //...
            self.pickerC.sourceType = .PhotoLibrary
            self.presentViewController(self.pickerC, animated: true, completion: nil)
        }
        alertController.addAction(PhotoLibraryAction)
        
        self.presentViewController(alertController, animated: true) {
            
        }
    }
    
    func goToCreateVC(){
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Create") as! CreateProfileViewController
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    func documentsPathForFileName(name: String) -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true);
        let path = paths[0] as String;
        let fullPath = path.stringByAppendingPathComponent(name)
        
        return fullPath
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
