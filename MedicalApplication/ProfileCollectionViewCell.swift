//
//  ProfileCollectionViewCell.swift
//  MedicalApplication
//
//  Created by Havic on 4/17/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//

import UIKit

class ProfileCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var fname: UILabel!
    @IBOutlet weak var lname: UILabel!
    @IBOutlet weak var profileThumb: UIImageView!
    
    var firstName = String?()
    var lastName = String?()
}
