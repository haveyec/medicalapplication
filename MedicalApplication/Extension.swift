//
//  Extension.swift
//  MedicalApplication
//
//  Created by Havic on 5/24/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//

import UIKit

extension String{
    
    var lastPathComponent: String {
        
        get {
            return (self as NSString).lastPathComponent
        }
    }
    var pathExtension: String {
        
        get {
            
            return (self as NSString).pathExtension
        }
    }
    var stringByDeletingLastPathComponent: String {
        
        get {
            
            return (self as NSString).stringByDeletingLastPathComponent
        }
    }
    var stringByDeletingPathExtension: String {
        
        get {
            
            return (self as NSString).stringByDeletingPathExtension
        }
    }
    var pathComponents: [String] {
        
        get {
            
            return (self as NSString).pathComponents
        }
    }
    
    func stringByAppendingPathComponent(path: String) -> String {
        
        let nsSt = self as NSString
        
        return nsSt.stringByAppendingPathComponent(path)
    }
    
    func stringByAppendingPathExtension(ext: String) -> String? {
        
        let nsSt = self as NSString
        
        return nsSt.stringByAppendingPathExtension(ext)
    }
}

extension UIImage{
    convenience init?(createFromThis:String?) {
        self.init(contentsOfFile: createFromThis!)
        return
        guard let urlString = createFromThis, let stringUrl = NSURL(string: urlString), let dataObject = NSData(contentsOfURL: stringUrl) else{self.init()
            return}
        self.init(contentsOfFile: urlString)
        //  self.init(data: dataObject)
        
    }
    
}


extension PassCodeDelegate{
    
    
    func passcodeSetting(notification:UILocalNotification, futureDate:NSDate){
        notification.alertBody = "You have not used the app in awhile, you have now been logged out"
        
        let futureDate = NSDate(timeIntervalSinceNow: 120.0)
        notification.fireDate = futureDate
        
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
    }
}
