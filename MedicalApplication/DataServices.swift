//
//  DataServices.swift
//  MedicalApplication
//
//  Created by Havic on 4/19/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//

import UIKit

class DataServices: NSObject {
    var dataArray = NSMutableArray()
    let configItems = AppConfig.instance

    
    func setUpData(){
        guard let tempArr = configItems.defaults.objectForKey("dataInfo") as? NSArray else{return}
        
        for values in tempArr as! [NSDictionary]{
         let dataObjects = DataObject(dataDict: values)
            dataArray.addObject(dataObjects)
        }
        
        
}

}
