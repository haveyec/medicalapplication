//
//  DataObject.swift
//  MedicalApplication
//
//  Created by Havic on 4/19/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//

import UIKit
import CoreData


class DataObject:NSObject {
    var user: String?
    var immunization: [String]?
    var problem: [String]?
    var socialHistory: [String]?
    var vital: [String]?
    var test: [String]?
    var userProfilePic: String?
    
    init(dataDict:NSDictionary) {
        
        
        self.user = dataDict["name"] as? String
        self.problem = dataDict["problem"] as? [String]
        self.socialHistory = dataDict["socialHistory"] as? [String]
        self.test = dataDict["tests"] as? [String]
        self.immunization = dataDict["immunization"] as? [String]
        self.vital = dataDict["vital"] as? [String]
        self.userProfilePic = dataDict["profileImage"] as? String
    }
}