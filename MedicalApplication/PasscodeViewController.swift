//
//  PasscodeViewController.swift
//  MedicalApplication
//
//  Created by Havic on 6/19/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//

import UIKit

class PasscodeViewController: UIViewController {
    var configItem = AppConfig.instance
    @IBOutlet weak var passcodeField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func passCodeSubmitBTN(sender: UIButton) {
        let storedPasscode = configItem.defaults.objectForKey("userPasscode") as! String
        guard passcodeField.text == storedPasscode else{return}
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Home") as! ViewController
            self.presentViewController(vc, animated: true, completion: nil)
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
