//
//  ViewController.swift
//  MedicalApplication
//
//  Created by Havic on 4/17/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//

import UIKit
import CoreData

class EditProfile: UIViewController {
    
}




//MARK: Home View Controller 

class ViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    let reuseIdentifier = "cell"
    var configItem = AppConfig.instance
    let datasvc = DataServices()
    var isLoggedIn: Bool?
    var profileDetailPage = ProfileDetailPageViewController()
    @IBOutlet weak var myCollection: UICollectionView!
    
    override func viewWillAppear(animated: Bool) {
        myCollection.reloadData()
        guard datasvc.dataArray.count == 0 else{return myCollection.alpha = 1}
        myCollection.alpha = 0
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("EmptySet") as! EmptySetVC
        self.presentViewController(vc, animated: true, completion: nil)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        isLoggedIn = configItem.defaults.boolForKey("loggedin")
        guard (isLoggedIn == false) else{return}
        datasvc.setUpData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Segue Method
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        let storyCell = sender as! ProfileCollectionViewCell
        profileDetailPage = segue.destinationViewController as! ProfileDetailPageViewController
        let indexPathVar: NSIndexPath = myCollection.indexPathForCell(storyCell)!
        let userContent = datasvc.dataArray[indexPathVar.row] as! DataObject
        
        guard let arrOfProblems = userContent.problem, arrOfVitals = userContent.vital, socials = userContent.socialHistory,testing = userContent.test, nameString = userContent.user, arrOfImmunizations = userContent.immunization, profilePic = userContent.userProfilePic else{return}
        
        profileDetailPage.personName = nameString
        profileDetailPage.problems.addObjectsFromArray(arrOfProblems)
        profileDetailPage.vitals.addObjectsFromArray(arrOfVitals)
        profileDetailPage.socialHistory.addObjectsFromArray(socials)
        profileDetailPage.tests.addObjectsFromArray(testing)
        profileDetailPage.immunization.addObjectsFromArray(arrOfImmunizations)
        let paths =     NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true);
        let path = paths[0] as String;
        let fullPath = path.stringByAppendingPathComponent(profilePic)
        let profileImage = UIImage(createFromThis: fullPath)
        
        profileDetailPage.avitar = profileImage!
//
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datasvc.dataArray.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath)  as! ProfileCollectionViewCell
        guard let dataContent = datasvc.dataArray[indexPath.row] as? DataObject else{return cell}
        cell.backgroundColor = UIColor.blackColor()
        cell.fname.textColor = UIColor.whiteColor()
        cell.fname.text = dataContent.user
        
        let paths =     NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true);
        let path = paths[0] as String;
        let fullPath = path.stringByAppendingPathComponent(dataContent.userProfilePic!)
        
        let profileImage = UIImage(createFromThis: fullPath)
        cell.profileThumb.image = profileImage
        
        return cell
    }
    
    
    @IBAction func addProfileBTN(sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Create") as! CreateProfileViewController
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func settingsBtnPressed(sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Settings") as! Settings
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    
    
}

