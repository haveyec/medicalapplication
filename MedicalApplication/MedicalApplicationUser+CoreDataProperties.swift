//
//  MedicalApplicationUser+CoreDataProperties.swift
//  MedicalApplication
//
//  Created by Havic on 4/24/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension MedicalApplicationUser {

    @NSManaged var firstName: String?
    @NSManaged var immunization: NSSet?
    @NSManaged var medicalproblem: NSSet?
    @NSManaged var socialhistory: NSSet?
    @NSManaged var test: NSSet?
    @NSManaged var vitals: NSSet?

}
