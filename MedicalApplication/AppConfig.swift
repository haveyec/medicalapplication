//
//  AppConfig.swift
//  MedicalApplication
//
//  Created by Havic on 4/17/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//

import UIKit
import CoreData

@objc protocol PassCodeDelegate {
   // func fireOffPassCodeTimer()
   optional func sendToPassCode()
    optional func passcodeSetting()
}

class AppConfig: NSObject {
    static let instance = AppConfig()
   var appDel = UIApplication.sharedApplication().delegate as? AppDelegate
    
    var defaults = NSUserDefaults.standardUserDefaults()
    var segementArr = ["Problem","Social-History", "Immunizations", "Tests","Vitals"]
    var segementArrForCreateProfile = ["Name","Problem","Social-History", "Immunizations", "Tests","Vitals"]
    
    //Method for setting up titles in a segemented controller.
    
    /**
    
    Segemented Controller Setup
    
    
    - parameter segmentControl: -> shared session object dataTaskWithUrl(NSURL!,{ (NSData, NSResponse, NSError)} )
    
    - parameter NSArray: This will hold the list of titles that we will loop through.
    
    - returns: The blogArray which is created from the info we temporaily stored it in tempArr
    
    */
    func segmentTitlesSetup(segmentControl:UISegmentedControl,titlesArray:NSArray?){
        segmentControl.removeAllSegments()
        
        for objects in titlesArray!{
            guard let titles = (objects as? String) else{return}
            segmentControl.insertSegmentWithTitle(titles, atIndex: segmentControl.numberOfSegments, animated: true)
        }
    }
    
    
    func displayShareSheetFromViewController(viewController viewController:UIViewController, personName:String, problems: String?, vitals: String?,socialHistory: String?,
    immunization: String?,tests: String?,profilePic: UIImageView?,anchorView: UIView?) {
        
        let sharingSignature = "| via My Medical app."
        guard let problemsString = problems, vitalsString = vitals,socialHistoryString = socialHistory,immunizationString = immunization, testsString = tests,userProfilePic = profilePic else{return}
        let activityViewController = UIActivityViewController(activityItems: [personName, problemsString, vitalsString,socialHistoryString,immunizationString,testsString,userProfilePic, sharingSignature], applicationActivities: nil)
        
        let excludeActivities = [UIActivityTypePostToTencentWeibo,UIActivityTypePostToWeibo,UIActivityTypePrint,UIActivityTypeSaveToCameraRoll,UIActivityTypePostToVimeo,UIActivityTypePostToFlickr,UIActivityTypeAssignToContact,UIActivityTypeAddToReadingList]
        
        activityViewController.excludedActivityTypes = excludeActivities
//        if SizeManager.isWideLayout == true {
//            guard let anchorView = anchorView else {
//                fatalError("need to pass an anchor view for iPad")
//            }
//            activityViewController.popoverPresentationController?.sourceView = anchorView
//            activityViewController.popoverPresentationController?.sourceRect = anchorView.bounds
//            activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.Down
//        }
        
        viewController.presentViewController(activityViewController, animated: true, completion: nil)
    }

}
