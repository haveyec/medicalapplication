//
//  CreateProfileViewController.swift
//  MedicalApplication
//
//  Created by Havic on 4/19/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//

import UIKit
import CoreData

class CreateProfileViewController: UIViewController {
    let config = AppConfig.instance
    var problemsArr = NSMutableArray()
    var socialHistory = NSMutableArray()
    var immunization = NSMutableArray()
    var test = NSMutableArray()
    var vital = NSMutableArray()
    var dataInfo = NSMutableArray()
    var nameStr = String()
    var myArray = NSMutableArray()
    var myImage = UIImage()
    
    @IBOutlet weak var segments: UISegmentedControl!
    
    @IBOutlet weak var profileDataTypeTitle: UILabel!
    
    @IBOutlet weak var profileDataTypeTextField: UITextField!
    
    @IBOutlet weak var submitProfile: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        config.segmentTitlesSetup(segments, titlesArray: config.segementArrForCreateProfile)
        profileDataTypeTitle.text = config.segementArrForCreateProfile[0]
        segments.selectedSegmentIndex = 0
        guard let something = config.defaults.objectForKey("dataInfo") as? [AnyObject] else{return}
        dataInfo.addObjectsFromArray(something)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: segments that write to NSUserDefaults
    
    @IBAction func profileDataTypeSaveBtn(sender: UIButton) {
        switch segments.selectedSegmentIndex{
        case 0:
            guard profileDataTypeTextField.text?.characters.count > 0 else{
                nameError()
            return
            }
            
            settingUpUserNameToBeSaved(nameStr, nameOfKey: "name")
            createProfilePicAlert()
            alertForAttributeAdded("\(nameStr) Name")
            
        case 1:
            settingUpDataToBeSaved(problemsArr, nameOfKey: "problem")
            alertForAttributeAdded("Problem")
        case 2:
            settingUpDataToBeSaved(socialHistory, nameOfKey: "social")
            alertForAttributeAdded("Social History item")
        case 3:
            settingUpDataToBeSaved(immunization, nameOfKey: "immunization")
            alertForAttributeAdded("Immunization item")
        case 4:
            settingUpDataToBeSaved(test, nameOfKey: "test")
            alertForAttributeAdded("Test item")
        case 5:
            settingUpDataToBeSaved(vital, nameOfKey: "vital")
            alertForAttributeAdded("Vitals item")
        default:
            
            break
        }
    }
    
    // MARK: Final save that will write to Core Data and clean up NSUserDefaults
    
    @IBAction func profileSaveBtn(sender: UIButton) {
        guard let name = config.defaults.objectForKey("name"), let profileImage = config.defaults.objectForKey("profileImage") as? String
            else{return}
        
        let problem = problemsArr
        let social = socialHistory
        let immunizationLit = immunization
        let testLit = test
        let vitalLit = vital
        
        
        let user = ["name": name, "problem":problem, "socialHistory":social, "tests":testLit, "immunization": immunizationLit,"vital": vitalLit,"profileImage": profileImage]
        
        dataInfo.addObject(user)
        config.defaults.setObject(dataInfo, forKey: "dataInfo")
        alertForUserAdded("Person")
      
    }
    
    // MARK: Segmented Control for tabs
    
    @IBAction func segmentsBtn(sender: UISegmentedControl) {
        
        switch segments.selectedSegmentIndex{
        case 0:
            profileDataTypeTitle.text = config.segementArrForCreateProfile[0]
            submitProfile.alpha = 1
        case 1:
            profileDataTypeTitle.text = config.segementArrForCreateProfile[1]
            submitProfile.alpha = 0
            profileDataTypeTextField.alpha = 1
        case 2:
            profileDataTypeTitle.text = config.segementArrForCreateProfile[2]
            submitProfile.alpha = 0
            profileDataTypeTextField.alpha = 1
        case 3:
            profileDataTypeTitle.text = config.segementArrForCreateProfile[3]
            submitProfile.alpha = 0
            profileDataTypeTextField.alpha = 1
        case 4:
            profileDataTypeTitle.text = config.segementArrForCreateProfile[4]
            submitProfile.alpha = 0
            profileDataTypeTextField.alpha = 1
        case 5:
            profileDataTypeTitle.text = config.segementArrForCreateProfile[5]
            submitProfile.alpha = 0
            profileDataTypeTextField.alpha = 1
        default:
            profileDataTypeTitle.text = config.segementArrForCreateProfile[0]
            submitProfile.alpha = 1
            profileDataTypeTextField.alpha = 0
            break
        }
    }
    
    
    
    // MARK: Helper Methods
    
    func settingUpDataToBeSaved(arrayOfData:NSMutableArray,nameOfKey:String){
        guard profileDataTypeTextField.text?.characters.count > 0 else{return}
        guard let someStr = profileDataTypeTextField.text else{return}
        arrayOfData.addObject(someStr)
        
        config.defaults.setValue(arrayOfData, forKey: nameOfKey)
        profileDataTypeTextField.text?.removeAll()
        profileDataTypeTextField.alpha = 1
    }
    
    func alertForAttributeAdded(nameOfThingSaved:String){
        let alertController = UIAlertController(title: "Added", message: "Save was successful can add another \(nameOfThingSaved) if you would like", preferredStyle: .Alert)
        
        
        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            // ...
        }
        alertController.addAction(OKAction)
        
        self.presentViewController(alertController, animated: true) {
            // ...
        }
    }
    
    func settingUpUserNameToBeSaved(name:String,nameOfKey:String){
        guard profileDataTypeTextField.text?.characters.count > 0 else{return}
        guard let name = profileDataTypeTextField.text else{return}
        
        config.defaults.setValue(name, forKey: nameOfKey)
        profileDataTypeTextField.text?.removeAll()
        profileDataTypeTextField.alpha = 0
    }
    
    func alertForUserAdded(nameOfThingSaved:String){
        let alertController = UIAlertController(title: "Added", message: "Save was successful can add another \(nameOfThingSaved) if you would like", preferredStyle: .Alert)
        
        
        let YesAction = UIAlertAction(title: "Yes", style: .Default) { (action) in
            // ...
        }
        alertController.addAction(YesAction)
        
        let NoAction = UIAlertAction(title: "No", style: .Default) { (action) in
            // ...
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Home") as! ViewController
            self.presentViewController(vc, animated: true, completion: nil)
        }
        alertController.addAction(NoAction)
        
        self.presentViewController(alertController, animated: true) {
            // ...
        }
    }
    
    func nameError(){
        let alertController = UIAlertController(title: "Error", message: "Can't leave this blank, everyone has a name", preferredStyle: .Alert)
        
        
        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            // ...
        }
        alertController.addAction(OKAction)
        
        self.presentViewController(alertController, animated: true) {
            // ...
        }
    }
    
    func createProfilePicAlert(){
        let alertController = UIAlertController(title: "Question", message: "Would you like to create a Profile Image for this person?", preferredStyle: .Alert)
        
        
        let YesAction = UIAlertAction(title: "Yes", style: .Default) { (action) in
            // Take user to Camera Controller for profile image
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Camera") as! ImageChoiceViewController
            self.presentViewController(vc, animated: true, completion: nil)
        }
        alertController.addAction(YesAction)
        
        let NOAction = UIAlertAction(title: "No", style: .Default) { (action) in
            //...
        }
        alertController.addAction(NOAction)
        
        self.presentViewController(alertController, animated: true) {
            // ...
        }
    }
    
}
