//
//  ProfileDetailPageViewController.swift
//  MedicalApplication
//
//  Created by Havic on 4/20/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//

import UIKit

class ProfileDetailPageViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var myTable: UITableView!
    var personName = String()
    let config = AppConfig.instance
    let reuseIdentifier = "cell"
    var problems = NSMutableArray()
    var vitals = NSMutableArray()
    var socialHistory = NSMutableArray()
    var immunization = NSMutableArray()
    var tests = NSMutableArray()
    let indexPar = NSIndexPath()
    var avitar = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //        config.segementArr.removeAtIndex(0)
        config.segmentTitlesSetup(segmentController, titlesArray: config.segementArr)
        profilePic.image = avitar
            //UIImage(named: "Be-Cool-Smile-whatsapp-dp.jpg")
        name.text = personName
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier, forIndexPath: indexPath)
        
        // Configure the cell...
        switch segmentController.selectedSegmentIndex{
        case 0:
            cell.textLabel?.text = problems[indexPath.row] as? String
            break
        case 1:
            cell.textLabel?.text = socialHistory[indexPath.row] as? String
            break
        case 2:
            cell.textLabel?.text = immunization[indexPath.row] as? String
        case 3:
            cell.textLabel?.text = tests[indexPath.row] as? String
        case 4:
            cell.textLabel?.text = vitals[indexPath.row] as? String
        default:
            cell.textLabel?.text = problems[indexPath.row] as? String
            break
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        switch segmentController.selectedSegmentIndex{
        case 0:
            return problems.count
        case 1:
            return socialHistory.count
        case 2:
            return immunization.count
        case 3:
            return tests.count
        case 4:
            return vitals.count
        default:
            break
        }
        
        return problems.count
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = NSIndexPath(index: indexPath.row)
        print("I touched this cell:",cell)
    }
    @IBAction func mySegmentController(sender: AnyObject) {
        myTable.reloadData()
    }
    
    
    @IBAction func shareBtn(sender: UIButton) {
        let problemsString = String(problems)
        let vitalsString = String(vitals)
        let socialHistorySting = String(socialHistory)
        let immunizationString = String(immunization)
        let testsString = String(tests)
        let usersPic = profilePic.image
        
        
        
        config.displayShareSheetFromViewController(viewController: self, personName: personName, problems: problemsString, vitals: vitalsString, socialHistory: socialHistorySting, immunization: immunizationString, tests: testsString,profilePic: UIImageView(image: usersPic), anchorView: sender)
    }
    
    
    @IBAction func editThatTable(sender: UIButton) {
        
        myTable.setEditing(editing, animated: true)
    }
    
    func tableView(tableView: UITableView, titleForDeleteConfirmationButtonForRowAtIndexPath indexPath: NSIndexPath) -> String? {
        return "Delete this"
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func insertRowsAtIndexPaths(indexPaths: [NSIndexPath], withRowAnimation animation: UITableViewRowAnimation){
        
    }
    func deleteRowsAtIndexPaths(indexPaths: [NSIndexPath], withRowAnimation animation: UITableViewRowAnimation){
        
    }
    
    func reloadRowsAtIndexPaths(indexPaths: [NSIndexPath], withRowAnimation animation: UITableViewRowAnimation){
        
    }
    
}
