//
//  EmptySetVC.swift
//  MedicalApplication
//
//  Created by Havic on 5/14/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//

import UIKit

class EmptySetVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func createProfileBtn(sender: UIButton) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Create") as! CreateProfileViewController
        self.presentViewController(vc, animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
