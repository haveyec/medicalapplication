//
//  Settings.swift
//  MedicalApplication
//
//  Created by Havic on 5/24/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//

import UIKit

class Settings: UIViewController {
    
    @IBOutlet weak var savebtn: UIButton!
    @IBOutlet weak var passcodeSwitch: UISwitch!
    @IBOutlet weak var newPasscodeCreateField: UITextField!
    @IBOutlet weak var newPasscodeCreateConfirmField: UITextField!
    
    var configItem = AppConfig.instance
    var passcodeIsEnabled = true
    
    override func viewDidLoad() {
        
        let passCode =  String(configItem.defaults.objectForKey("passcodeIsEnabled"))
        
        if passCode == "true"{
            passcodeSwitch.on = true
            newPasscodeCreateField.hidden = false
            newPasscodeCreateConfirmField.hidden = false
            savebtn.hidden = false
        }else{
            passcodeSwitch.on = false
            newPasscodeCreateField.hidden = true
            newPasscodeCreateConfirmField.hidden = true
            savebtn.hidden = true
        }
        
    }
    
    @IBAction func passcodeSwitchBtn(sender: UISwitch) {
        
        if passcodeSwitch.on == true{
            newPasscodeCreateField.hidden = false
            newPasscodeCreateConfirmField.hidden = false
            savebtn.hidden = false
            let trueValue = "true"
            configItem.defaults.setObject(trueValue, forKey: "passcodeIsEnabled")
            print(configItem.defaults.objectForKey("passcodeIsEnabled"))
        }else{
            newPasscodeCreateField.hidden = true
            newPasscodeCreateConfirmField.hidden = true
            savebtn.hidden = true
            let falseValue = "false"
            configItem.defaults.setObject(falseValue, forKey: "passcodeIsEnabled")
            print(configItem.defaults.objectForKey("passcodeIsEnabled"))
        }
        
    }
    
    @IBAction func newPasscodeSaveBtn(sender: UIButton) {
        guard newPasscodeCreateField.text == newPasscodeCreateConfirmField.text else{return}
        let userPasscode = newPasscodeCreateConfirmField.text
        configItem.defaults.setObject(userPasscode, forKey: "userPasscode")
    }
    
}
