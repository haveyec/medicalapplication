//
//  Testprocedure+CoreDataProperties.swift
//  MedicalApplication
//
//  Created by Havic on 4/24/16.
//  Copyright © 2016 Nerd Gang LLC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Testprocedure {

    @NSManaged var test: String?
    @NSManaged var user: MedicalApplicationUser?

}
